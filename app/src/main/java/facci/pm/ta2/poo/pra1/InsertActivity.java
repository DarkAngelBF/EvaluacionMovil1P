package facci.pm.ta2.poo.pra1;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import facci.pm.ta2.poo.datalevel.DataException;
import facci.pm.ta2.poo.datalevel.DataObject;
import facci.pm.ta2.poo.datalevel.SaveCallback;

public class InsertActivity extends AppCompatActivity {

    Button buttonCamara, buttonInsert;
    ImageView imageViewFoto;
    Bitmap imageBitmap;
    EditText editTextNombre;
    EditText editTextPrecio;
    EditText editTextDescripcion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        buttonCamara = (Button) findViewById(R.id.buttonTomarFoto);
        buttonInsert = (Button) findViewById(R.id.buttonInsert);
        imageViewFoto = (ImageView) findViewById(R.id.imageViewFoto);
        editTextNombre = (EditText) findViewById(R.id.EditTextNombre);
        editTextPrecio= (EditText) findViewById(R.id.EditTextPrecio);
        editTextDescripcion = (EditText) findViewById(R.id.EditTextDescripcion);
        buttonCamara.setOnClickListener(new View.OnClickListener() {
            static final int REQUEST_IMAGE = 1;
            @Override

            public void onClick(View v) {
                Intent abrirCamara = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if(abrirCamara.resolveActivity(getPackageManager())!=null){
                    startActivityForResult(abrirCamara,REQUEST_IMAGE);
                }
            }
        });

                buttonInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final DataObject objeto = new DataObject("Item");
                String Name = editTextNombre.getText().toString();
                String Price = editTextPrecio.getText().toString();
                String Descripcion = editTextDescripcion.getText().toString();
                imageViewFoto.buildDrawingCache();
                Bitmap Bmap = imageViewFoto.getDrawingCache();
                objeto.put("Name", Name);
                objeto.put("Description", Descripcion);
                objeto.put("Price", Price);
                objeto.put("Image", Bmap);
                objeto.saveInBackground(new SaveCallback<DataObject>() {

                    @Override
                    public void done(DataObject object, DataException e) {
                        Toast.makeText(getApplicationContext(),"Foto insertada correctamente", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(InsertActivity.this, ResultsActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),0);
        return super.onTouchEvent(event);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final int REQUEST_IMAGE = 1;
        if (requestCode ==  REQUEST_IMAGE && resultCode == RESULT_OK){
            Bundle extras = data.getExtras();
            imageBitmap = (Bitmap) extras.get("Date");
            imageViewFoto.setImageBitmap(imageBitmap);

        }
    }
}


